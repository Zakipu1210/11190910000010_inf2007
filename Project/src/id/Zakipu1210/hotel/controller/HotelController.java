package id.Zakipu1210.hotel.controller;

import com.google.gson.Gson;
import id.Zakipu1210.AplikasiHotel1.MODEL.Hotel;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Zaki
 */
public class HotelController {

    private static final String FILE = "C:\\Users\\Ghost\\Documents\\NetBeansProjects\\AplikasiHotel1\\lib\\hotelx.json";
    private Hotel hotelx;
    private String nama;
    private int noKTP;
    private int hari;
    private int Tipe;
    private String noKamar;
    private String request;
    private final LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private final Scanner in;
    private Object getDataHotel;

    public HotelController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        waktuKeluar = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HM:mm:ss");
    }

    /**
     * Method untuk menginput data masuk Hotel
     */
    public void setMasukHotel() {
        System.out.println("Masukkan Nama Anda");
        nama = in.next();
        System.out.println("Masukkan noKTP : ");
        noKTP = in.nextInt();
        System.out.println("Tipe Kamar");
        System.out.println("1. Luxury Class");
        System.out.println("2. Deluxe Class");
        System.out.println("3. Suite Class");
        System.out.println("4. Reguler Class");
        System.out.print("Pilih Tipe Kamar : ");
        Tipe = in.nextInt();
        System.out.println("Berapa Lama Anda Menginap?");
        hari = in.nextInt();
        System.out.println("Apa Ada Sesuatu Yang Anda Butuhkan? ");
        request = in.next();
        System.out.println("Input Nomor Kamar Yang Tersedia");
        noKamar = in.next();
        String WaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + WaktuMasuk);
        hotelx = new Hotel();
        hotelx.setNama(nama);
        hotelx.setNoKTP(noKTP);
        hotelx.setTipe(Tipe);
        hotelx.setNoKamar(noKamar);
        hotelx.setHari(hari);
        hotelx.setRequest(request);

        setWriteHotel(FILE, hotelx);
        System.out.println("Apakah mau Input kembali?");
        System.out.println("1) Ya, 2) Tidak");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukHotel();
        }
    }

    public void setWriteHotel(String file, Hotel hotelx) {
        Gson gson = new Gson();

        List<Hotel> x = getReadHotel(file);
        x.remove(hotelx);
        x.add(hotelx);
        String json = gson.toJson(x);
        try {
            try (FileWriter writer = new FileWriter(file)) {
                writer.write(json);
            }
        } catch (IOException ex) {
            Logger.getLogger(HotelController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setKeluarHotel() {
        System.out.println("Masukkan nomor Kamar");
        noKamar = in.next();

        Hotel h = getSearch(noKamar);
        if (h != null) {
            waktuKeluar = LocalDateTime.now();
            if (h.getHari() > 1) {
                biaya = new BigDecimal(h.getHari());
                if (h.getTipe() == 1) {
                    biaya = biaya.multiply(new BigDecimal(10000000));
                } else if (h.getTipe() == 2) {
                    biaya = biaya.multiply(new BigDecimal(8000000));
                } else if (h.getTipe() == 3) {
                    biaya = biaya.multiply(new BigDecimal(5000000));
                } else if (h.getTipe() == 4) {
                    biaya = biaya.multiply(new BigDecimal(2000000));
                }else{
                    System.out.println("harga tidak ada");
                }
                } else {
                    hari = 1;
                    if (h.getTipe() == 1) {
                        biaya = new BigDecimal(10000000);
                    } else if (h.getTipe() == 2) {
                        biaya = new BigDecimal(8000000);
                    } else if (h.getTipe() == 3) {
                        biaya = new BigDecimal(5000000);
                    } else if (h.getTipe() == 4) {
                        biaya = new BigDecimal(200000);
                    } else {
                        System.out.println("Data tidak ditemukan");
                    }
                }
            
            h.setBiaya(biaya);
            h.setKeluar(true);
            System.out.println(" Nama : " + h.getNama());
            System.out.println("No.KTP : " + h.getNoKTP());
            System.out.println("Tipe : " + h.getTipe());
            System.out.println("WaktuKeluar : " + h.getWaktuKeluar());
            System.out.println("Biaya : " + biaya);

            System.out.println("Proses Bayar?");
            System.out.println("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    setWriteHotel(FILE, h);
                    break;
                case 2:
                    setKeluarHotel();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;

            }
            System.out.println("Apakah mau memproses kembali?");
            System.out.println("1)Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setKeluarHotel();
            }
        } else {
            System.out.println("Data Tidak ditemukan");
            setKeluarHotel();
        }
    }

    public Hotel getSearch(String noKamar) {
        List<Hotel> x = getReadHotel(FILE);

        Hotel h = x.stream()
                .filter(pp -> noKamar.equalsIgnoreCase(pp.getNoKamar()))
                .findAny()
                .orElse(null);

        return h;
    }
    /**
     *
     * @param file
     * @return
     */
    public List<Hotel> getReadHotel (String file) {
        List<Hotel> x = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
       try (Reader y = new FileReader(file); BufferedReader read = new BufferedReader(y)) {
           while ((line = read.readLine())!= null) {
               Hotel[] ps = gson.fromJson(line, Hotel[].class);
               x.addAll(Arrays.asList(ps));
           }
       } catch (FileNotFoundException ex) {
           Logger.getLogger(HotelController.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(HotelController.class.getName()).log(Level.SEVERE, null, ex);
       }
        return x;
    }

    public void getDataHotel() {
        List<Hotel> x = getReadHotel(FILE);
        Predicate<Hotel> isKeluar = e -> e.isKeluar() == true;
      
        
        
        List<Hotel> hResults = x.stream().filter(isKeluar).collect(Collectors.toList());
        BigDecimal total = hResults.stream()
                .map(Hotel::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        hResults.forEach((h) -> {
            System.out.println(" Nama : " + h.getNama());
            System.out.println("No.KTP : " + h.getNoKTP());
            System.out.println("Tipe : " + h.getTipe());
        });
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        }else{
            getDataHotel();
        }
    }
}
    