package id.Zakipu1210.hotel.controller;

import id.Zakipu1210.AplikasiHotel1.MODEL.Info;
import java.util.Scanner;

/**
 *
 * @author Zaki
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1.Menu Masuk Hotel");
        System.out.println("2.Menu Keluar Hotel");
        System.out.println("3.Laporan Harian");
        System.out.println("4.Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.println("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.println("Pilih menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu(); 

    }
    
    public void setPilihMenu() {
        HotelController pc = new HotelController();
        switch (noMenu) {
            case 1:
                pc.setMasukHotel();
                break;
            case 2:
                pc.setKeluarHotel();
            case 3:
                pc.getDataHotel();
                break;
            case 4:
                System.out.println("Good Bye :)");
                System.exit(0);
                break;
        }
    }

}
