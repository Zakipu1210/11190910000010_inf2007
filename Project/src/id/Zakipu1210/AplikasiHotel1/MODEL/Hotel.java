package id.Zakipu1210.AplikasiHotel1.MODEL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
/**
 *
 * @author Zaki
 */
public class Hotel implements Serializable {

    private static final long serialVersionUID = -99999999999999999L;

    private int noKTP;
    private String nama;
    private int hari;
    private int Tipe;
    private String noKamar;
    private LocalDateTime waktuMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;

    public int getHari() {
        return hari;
    }

    public void setHari(int hari) {
        this.hari = hari;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    
    public int getNoKTP() {
        return noKTP;
    }

    public void setNoKTP(int noKTP) {
        this.noKTP = noKTP;
    }

    public String getNoKamar() {
        return noKamar;
    }

    public void setNoKamar(String noKamar) {
        this.noKamar = noKamar;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
    private String request;
    private boolean keluar = false;

    public Hotel() {

    }

    public Hotel(int hari, String nama, String request, int noKTP, int Tipe, String noKamar,
            LocalDateTime waktuMasuk, LocalDateTime waktuKeluar, BigDecimal biaya) {
        this.noKTP = noKTP;
        this.Tipe = Tipe;
        this.noKamar = noKamar;
        this.waktuMasuk = waktuMasuk;
        this.waktuKeluar = waktuKeluar;
        this.biaya = biaya;
        this.nama = nama;
        this.hari = hari;
        this.request = request;
    }

    public int getnoKTP() {
        return noKTP;
    }

    public void setnoKTP(int noKTP) {
        this.noKTP = noKTP;
    }

    public int getTipe() {
        return Tipe;
    }

    public void setTipe(int Tipe) {
        this.Tipe = Tipe;
    }

    public String getnoKamar() {
        return noKamar;
    }

    public void setnoKamar(String noKamar) {
        this.noKamar = noKamar;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;

    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public LocalDateTime getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(LocalDateTime waktuKeluar) {
        this.waktuKeluar = waktuKeluar;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    @Override
    public String toString() {
        return "Hotel{" + "nama" + nama + "noKTP=" + noKTP + ", Tipe=" + Tipe + ",noKamar=" + noKamar + ", waktuMasuk=" + waktuMasuk + ", waktuKeluar=" + waktuKeluar + "hari = " + hari + ",biaya=" + biaya + ",keluar=" + keluar + "request " + request + '}';
    }
}
