
package id.Zakipu1210.pertemuan.keduabelas;

/**
 *
 * @author Zaki
 */
public class PenyimpananUang extends Tabungan1 {
    private double tingkatBunga;
    
    public PenyimpananUang (int saldo, double tingkatBunga) {
        super(saldo);
        this.tingkatBunga = tingkatBunga;
    }
    public double cekUang () {
        return saldo + (saldo + tingkatBunga);
    }
    
}
