package id.Zakipu1210.pertemuan.keduabelas;

/**
 *
 * @author Zaki
 */
public class Kalender {

    private int tanggal;
    private int bulan;
    private int tahun;

    public int getTanggal() {
        return tanggal;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public int getBulan() {
        return bulan;
    }

    public void setBulan(int bulan) {
        this.bulan = bulan;
    }

    public int getTahun() {
        return tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public Kalender(int tanggal) {
        this.bulan = bulan;
        this.tahun = tahun;
        this.tanggal = 1;

    }

    public Kalender(int tanggal, int bulan, int tahun) {
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
    }

}
