
package id.Zakipu1210.pertemuan.keduabelas;

/**
 *
 * @author Zaki
 */
public class KalenderTest {
    public static String getTime(Kalender kalender) {
        return kalender.getTanggal() + "-" + kalender.getBulan() + "-" + kalender.getTahun();
    }
    
    public static void main(String[] args) {
        Kalender k = new Kalender (8);
        System.out.println("Waktu awal : " + getTime(k));
        k.setTanggal(9);
        System.out.println("1 hari setelah waktu awal : " + getTime(k));
        System.out.println(" Waktu berubah : " + getTime(k));
        k.setBulan(7);
        System.out.println("1 bulan setelah itu : " +  getTime(k));
        k.setTahun(2023);
        System.out.println("1 tahun setelah itu : " + getTime(k));
    }
    
}
