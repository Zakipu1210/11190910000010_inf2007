
package id.Zakipu1210.pertemuan.keduabelas;

/**
 *
 * @author Zaki
 */
public class Bayaran {
    public int hitungBayaran (Pegawai pegawai) {
        int uang = pegawai.infoGaji();
        if (pegawai instanceof Manager) {
            uang += ((Manager) pegawai).infoTunjangan();
        } else if (pegawai instanceof Programer) {
            uang += ((Programer)pegawai).infoBonus();
        }
        return uang;
    }
    public static void main(String[] args) {
        Manager m = new Manager ("Budi", 800, 50);
        Programer p = new Programer ("Cecep", 600, 300);
        Bayaran upah = new Bayaran ();
        System.out.println("Upah Manager : " + upah.hitungBayaran(m));
        System.out.println("Upah Programer : " + upah.hitungBayaran(p));
    }
    
}
