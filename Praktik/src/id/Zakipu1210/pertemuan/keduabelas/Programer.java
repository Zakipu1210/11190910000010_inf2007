
package id.Zakipu1210.pertemuan.keduabelas;

/**
 *
 * @author Zaki
 */
public class Programer extends Pegawai {
    private int bonus;
    
    public Programer(String nama, int gaji, int bonus) {
        super (nama, gaji);
        this.bonus = bonus;
        
    }
    public int infoGaji () {
        return this.gaji;
    }
    public int infoBonus () {
        return this.bonus;
    }
}
