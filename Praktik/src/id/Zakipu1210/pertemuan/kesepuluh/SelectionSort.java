
package id.Zakipu1210.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Zaki
 */
public class SelectionSort {
    int[] getSelectionSortMax(int[] L, int n) {
        int i, j, imaks, temp;
        
        for (i = n - 1; i > 0 ; i--) {
            imaks = 0;
            for (j = 1; j < i + 1; j++) {
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
             temp = L[i];
             L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }
    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21,};
        int n = L.length;
        SelectionSort app = new SelectionSort();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMax(L, n);
        System.out.println("\nHasil pengurutan Array");
        System.out.println(Arrays.toString(L));
    }

    void getSelectionSortMin(int[] L, int n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}