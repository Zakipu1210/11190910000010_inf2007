package id.Zakipu1210.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Zaki
 */
public class SelectionMinimum {

    int[] getSelectionSortMin(int[] L, int n) {
        int i, j, imin, temp;

        for (i = 0; i < n - 1; i++) {
            imin = i;

            for (j = i + 1; j < n; j++) {
                if (L[j] < L[imin]) {
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;

    }

    public static void main(String[] args) {
        int[] L = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        SelectionSort app = new SelectionSort();

        System.out.println("Array awal");
        System.out.println(Arrays.toString(L));
        app.getSelectionSortMin(L, n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
    }
}
