
package id.Zakipu1210.pertemuan.kesembilan;

/**
 *
 * @author Zaki
 */
public class BinarySearch {
    public int getBinarySearch(int L[], int n, int x) {
    int i = 0, j = n - 1, k=0;
    
    boolean ketemu = false;
    while (( i <= j) && (!ketemu)) {
        k = (i + j) /2;
        if (L[k] == x) {
           ketemu = true;
        }
        else {
            if (L[k] < x) {
                i = k + 1;
                
            } else {
                j = k -1;
            }
        }
    }
    if (ketemu) {
        return k;
    } else {
        return -1;
    }
    }
}




   

