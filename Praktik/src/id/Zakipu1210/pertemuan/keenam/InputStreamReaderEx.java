
package id.Zakipu1210.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Zaki
 */
public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader (new InputStreamReader(System.in));
        
        System.out.println("Masukan bilangan:");
        try {
            bilangan = Integer.parseInt(in.readLine());
        } catch (IOException e) {
            System.out.println("error:" + e.toString());
        }
        
    }
    
}
