
package id.Zakipu1210.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Zaki
 */
public class AplikasiPersegiPanjang {
    public static void main(String[] args) {
        //mencoba konstruktor default
        persegipanjang persegiPanjang2 = new persegipanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.print("Masukan Panjang");
        panjang = in.nextDouble();
        
        System.out.print("Masukan Lebar");
        lebar = in.nextDouble();
        persegipanjang persegiPanjang = new persegipanjang (panjang, lebar);
        persegiPanjang.getInfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());
        
    }
}
