
package id.Zakipu1210.pertemuan.ketujuh;

/**
 *
 * @author Zaki
 */
public class persegipanjang {
    private double panjang;
    private double lebar;
    
    public persegipanjang() {
        System.out.println("Kontruksi Persegi Panjang");
    }
    public persegipanjang(double panjang,double lebar){
        this.panjang = panjang;
        this.lebar = lebar;
    }
    public double getLuas() {
        return panjang * lebar;
    }
     public double getKeliling() {
         return 2 * (panjang + lebar);
     }
     public void getInfo() {
         System.out.println("Kelas Persegi Panjang");
     }
    
    
}
