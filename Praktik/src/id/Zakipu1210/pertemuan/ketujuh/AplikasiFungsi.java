
package id.Zakipu1210.pertemuan.ketujuh;

/**
 *
 * @author Zaki
 */
public class AplikasiFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi fungsi = new Fungsi ();
        
        System.out.println("--------------------");
        System.out.println("         f (x)      ");
        System.out.println("--------------------");
        
        x = (float)10.0;
        while (x <= 15.0) {
            System.out.println(x +" " + fungsi . f(x));
            x = x + (float) 0.2;
        }
        System.out.println("--------------------");
    }
    
}
