
package id.Zakipu1210.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Zaki
 */
public class AplikasiBulan {
    public static void main(String[] args) {
        int noBulan;
        Bulan namaBulan = new Bulan();
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan noBulan");
        noBulan = in.nextInt();
        
        System.out.println(namaBulan.getBulan(noBulan));
    }
    
}
