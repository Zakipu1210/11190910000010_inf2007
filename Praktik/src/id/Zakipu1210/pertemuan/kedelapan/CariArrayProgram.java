package id.Zakipu1210.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Zaki
 */
public class CariArrayProgram {

    public static void main(String[] args) {
        int x, n, i, j;
        Scanner in = new Scanner(System.in);
        CariArray app = new CariArray();

        System.out.print("N = ");
        n = in.nextInt();
        int[] nilai = new int[n];

        for (i = 0; i < nilai.length; i++) {
            System.out.print("masukkan Array [" + i + "] : ");
            nilai[i] = in.nextInt();
        }
        System.out.print("masukkan angka yang dicari: ");
        x = in.nextInt();
        app.getCari(nilai, x);

    }

}
