
package id.Zakipu1210.pertemuan.kedelapan;

/**
 *
 * @author Zaki
 */
public class Mahasiswa{
    private int noKTP;
    private String nama;
    private double TipeKamar;

    public int getnoKTP() {
        return noKTP;
    }

    public void setnoKTP(int nOKTP) {
        this.noKTP = noKTP;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getTipeKamar() {
        return TipeKamar;
    }

    public void setTipeKamar(double TipeKamar) {
        this.TipeKamar = TipeKamar;
    }

    public Mahasiswa(int noKTP, String nama, double TipeKamar) {
        this.noKTP = noKTP;
        
        this.nama = nama;
        this.TipeKamar = TipeKamar;
    }

    
}
