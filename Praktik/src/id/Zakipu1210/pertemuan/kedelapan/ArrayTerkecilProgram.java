package id.Zakipu1210.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Zaki
 */
public class ArrayTerkecilProgram {

    public static void main(String[] args) {
        int i, n;
        Scanner in = new Scanner(System.in);
        ArrayTerkecil app = new ArrayTerkecil();

        System.out.print("N = ");
        n = in.nextInt();
        int[] A = new int[n];

        for (i = 0; i < A.length; i++) {
            System.out.print("masukkan array [" + i + "] : ");
            A[i] = in.nextInt();

        }
        System.out.println("\nmatriks awal");
        for (i = 0; i < A.length; i++) {
            System.out.print(A[i] +" ");
        }
        app.getTerkecil(A, n);

    }

}
