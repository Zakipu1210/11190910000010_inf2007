
package id.Zakipu1210.pertemuan.kedelapan;

/**
 *
 * @author Zaki
 */
public class CariArray {
    int i, j = -1;
    
    public int getCari (int nilai[], int angka) {
        System.out.println("nilai pada Array");
        for (i = 0; i < nilai.length; i++) {
            System.out.print(nilai[i] +  " ");
        }
        
        System.out.print("");
        for (i = 0; i < nilai.length; i++) {
            if (angka == nilai[i]) {
                j = i;
                System.out.print("angka yang dicari (" + angka + ")");
                System.out.println(" berada pada indeks ke; " + j);
            }
        }
        if (j == -1) {
            System.out.println("0");
        }
        return j;
    }
    
}
